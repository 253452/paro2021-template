#include "gtest/gtest.h"
#include "day-of-year.hpp"

struct DayOfYearTestSuite {};

TEST(DayOfYearTestSuite, dummyTest)
{
  ASSERT_TRUE(true);
}

TEST(DayOfYearTestSuite, January1stIsFitstDayOfYear)
{
  ASSERT_EQ(dayOfYear(1, 1, 2020), 1);
}

TEST(DayOfYearTestSuite, March1stofLeapYearHas61Days)
{
    ASSERT_EQ(dayOfYear(3, 1, 1996), 61);
}

TEST(DayOfYearTestSuite, September1ofNoLeapYearHas244)
{
    ASSERT_EQ(dayOfYear(9, 1, 2021), 244);
}
