#include "day-of-year.hpp"

bool isLeapYear(int year)
{
    return ( year % 4 == 0 && year % 100 != 0 ) || (year % 400 == 0 );
}

int dayOfYear(int month, int dayOfMonth, int year) {
    int daySum = dayOfMonth;
    for (int m = 1; m < month; m++)
    {
        int offset = m > 7 ? 1:0;
        int monthDays = 30 + ((m + offset) % 2);
        if ( m == 2 )
        {
            if (isLeapYear(year))
                monthDays = 29;
            else
                monthDays = 28;
        }

        daySum += monthDays;
    }
    return daySum;
}

