#include "scorer.hpp"
#include <map>
#include <algorithm>
#include <cctype>
#include <iostream>

std::map<char, int> scoring;

void fillMap (std::string chars, int value)
{
    std::for_each(chars.begin(), chars.end(), [value](char c){
        scoring[c] = value;
    });
}

int score(std::string word)
{
    int sum = 0;

    std::map<char, int> occurences;


    static bool initialized;
    if(!initialized){
        initialized = true;
        fillMap("AEIOULNRST",1);
        fillMap("DG",2);
        fillMap("BCMP",3);
        fillMap("FHVWY",4);
        fillMap("K", 5);
        fillMap("JX", 8);
        fillMap("QZ", 10);
    }
    std::for_each(word.begin(), word.end(), [&sum, &occurences](char c ){
        c = toupper(c);
        sum += scoring[c];
        if(occurences.find(c) == occurences.end()){
            occurences[c] = 1;
        }
        else{
            occurences[c]++;
        }
    });
    std::for_each(occurences.begin(), occurences.end(), [&sum](std::pair<char, int> pair){
        if(pair.second == 2)
            sum += 10;
        if(pair.second > 2)
            sum += 20;
    });
    return sum;
}
//Letter                           Value
//A, E, I, O, U, L, N, R, S, T       1
//D, G                               2
//B, C, M, P                         3
//F, H, V, W, Y                      4
//K                                  5
//J, X                               8
//Q, Z                               10
