#include "gtest/gtest.h"
#include "scorer.hpp"

struct ScoringTestSuite{};

TEST(ScoringTestSuite, coldShouldScore7)
{
ASSERT_EQ(score("cold"), 7);
}

TEST(ScoringTestSuite, cabbageShouldScore34WithBonuses)
{
    ASSERT_EQ(score("cabbage"), 34);
}

TEST(ScoringTestSuite, ananasShouldScore36WithBonuses)
{
ASSERT_EQ(score("ananas"), 36);
}
//Letter                           Value
//A, E, I, O, U, L, N, R, S, T       1
//D, G                               2
//B, C, M, P                         3
//F, H, V, W, Y                      4
//K                                  5
//J, X                               8
//Q, Z                               10
